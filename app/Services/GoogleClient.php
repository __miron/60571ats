<?php


namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('733734946125-h34u99bq6a5rcefn56b3h2rirjfc24uh.apps.googleusercontent.com');
        $this->google_client->setClientSecret('yiH1j3F4SNVOjxiLpczs-K32');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}
