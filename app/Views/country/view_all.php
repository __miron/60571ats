<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все рейтинги</h2>

        <?php if (!empty($country) && is_array($country)) : ?>

        <div class="row">

            <?php foreach ($country as $item): ?>

                    <div class="card mb-3 col-md-4">
                        <div class="row">
                            <?php if (strlen($item['picture_url']) > 0) : ?>
                                <div class="col-md-8 ml-4 d-flex align-items-center">
                                    <img src="<?= esc($item['picture_url']); ?>" height="150" class="card-img" alt="<?= esc($item['наименование']); ?>">
                                </div>
                            <?php else:?>
                                <div class="col-md-8 ml-4 d-flex align-items-center">
                                    <img src="<?= esc($item['флаг']); ?>" height="150" class="card-img" alt="<?= esc($item['наименование']); ?>">
                                </div>
                            <?php endif ?>   
                            <div class="col-md-7">
                                <div class="card-body float-md-right">
                                    <h5 class="card-title"><?= esc($item['наименование']); ?></h5>
                                    <a href="<?= base_url()?>/index.php/country/view/<?= esc($item['id']); ?>" class="btn btn-primary mt-4">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php endforeach; ?>

        </div>

        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>
