<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($country)) : ?>
            <div class="card mb-3">
                <div class="row">
                    <?php if (strlen($country['picture_url']) > 0) : ?>
                        <div class="col-md-8 ml-4 d-flex align-items-center">
                            <img src="<?= esc($country['picture_url']); ?>" height="150" class="card-img" alt="<?= esc($country['наименование']); ?>">
                        </div>
                    <?php else:?>
                        <div class="col-md-8 ml-4 d-flex align-items-center">
                            <img src="<?= esc($country['флаг']); ?>" height="150" class="card-img" alt="<?= esc($country['наименование']); ?>">
                        </div>
                    <?php endif ?>
                    <div class="col-md-7">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($country['наименование']); ?></h5>
                            <p class="card-text"><?= esc($country['столица']); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Население:</div>
                                <div class="text-muted"><?= esc($country['население']); ?> чел.</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Площадь:</div>
                                <div class="text-muted"><?= esc($country['площадь']); ?> кв. км</div>
                            </div>
                        </div>
                        <div class="card-body float-md-right">
                            <a href="<?= base_url()?>/country/edit/<?= esc($country['id']); ?>" class="btn btn-primary mt-4">Редактировать</a>
                            <a href="<?= base_url()?>/country/delete/<?= esc($country['id']); ?>" class="btn btn-primary mt-4">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Рейтинг не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>