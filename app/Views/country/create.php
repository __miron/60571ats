<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('country/store'); ?>
    <div class="form-group">
        <label for="name">Страна</label>
        <input type="text" class="form-control <?= ($validation->hasError('наименование')) ? 'is-invalid' : ''; ?>" name="наименование"
               value="<?= old('наименование'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('наименование') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Столица</label>
        <input type="text" class="form-control <?= ($validation->hasError('столица')) ? 'is-invalid' : ''; ?>" name="столица"
               value="<?= old('столица'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('столица') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Население</label>
        <input type="text" class="form-control <?= ($validation->hasError('население')) ? 'is-invalid' : ''; ?>" name="население"
               value="<?= old('население'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('население') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Площадь</label>
        <input type="text" class="form-control <?= ($validation->hasError('площадь')) ? 'is-invalid' : ''; ?>" name="площадь"
               value="<?= old('площадь'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('площадь') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Флаг</label>
        <input type="text" class="form-control <?= ($validation->hasError('флаг')) ? 'is-invalid' : ''; ?>" name="флаг"
               value="<?= old('флаг'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('флаг') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


</div>
<?= $this->endSection() ?>
