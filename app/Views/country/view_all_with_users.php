<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main">
<!-- ?php
    $mysqli = new mysqli(getenv('database.default.hostname'), getenv('database.default.username'), getenv('database.default.password'), getenv('database.default.database'));
    $res = $mysqli->query("SELECT * FROM страна INNER JOIN users ON страна.user_id = users.id");
? -->

<?php if (!empty($country) && is_array($country)) : ?>
    <div class="d-flex justify-content-between mb-2">
        <?= $pager->links('group1','my_page') ?>
        <?= form_open('country/viewAllWithUsers', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('country/viewAllWithUsers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>
    <h2>Все страны:</h2>
    <table class="table table-striped">
        <thead>
            <th scope="col">Флаг</th>
            <th scope="col">Страна</th>
            <th scope="col">Столица</th>
            <th scope="col">Управление</th>
        </thead>
        <tbody>
    <?php foreach ($country as $item): ?>
        <tr>
        <td>
            <?php if (strlen($item['picture_url']) > 0) : ?>
                <img src="<?= esc($item['picture_url']); ?>" height="30" alt="<?= esc($item['наименование']); ?>">
            <?php else:?>
                <img src="<?= esc($item['флаг']); ?>" height="30" alt="<?= esc($item['наименование']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['наименование']); ?></td>
        <td><?= esc($item['столица']); ?></td>
        <td>
            <a href="<?= base_url()?>/country/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
            <a href="<?= base_url()?>/country/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
            <a href="<?= base_url()?>/country/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
        </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>
<?php else : ?>
    <div class="text-center">
    <p>Ничего не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/country/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Добавить страну</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
