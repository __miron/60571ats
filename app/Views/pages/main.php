<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="<?= base_url()?>/src/home.svg" alt="" width="72" height="72"><h1 class="display-4">CountryAnalytics</h1>
        <p class="lead">Это приложение поможет Вам анализировать экономическое состояние разных стран по всему миру.</p>
        <?php if (!$ionAuth->loggedIn()): ?>
            <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
        <?php else: ?>
            <a class="btn btn-primary btn-lg" href="auth/logout" role="button">Выйти</a>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>

