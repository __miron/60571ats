<?php namespace App\Models;
use CodeIgniter\Model;
class CountryModel extends Model
{
    protected $table = 'country'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['наименование', 'столица', 'население', 'площадь', 'флаг','user_id', 'picture_url'];

    public function getCountry($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getCountryWithUser($id = null, $search = '')
    {
        $builder = $this->select('*')->like('country.наименование', $search,'both', null, true)->orlike('country.столица',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['country.id' => $id])->first();
        }
        return $builder;
    }
}

