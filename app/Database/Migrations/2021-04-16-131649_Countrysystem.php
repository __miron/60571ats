<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Countrysystem extends Migration
{
	public function up()
	{
        if (!$this->db->tableexists('country'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'наименование' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'столица' => array('type' => 'TEXT', 'null' => TRUE),
                'население' => array('type' => 'INT', 'null' => TRUE),
                'площадь' => array('type' => 'FLOAT', 'null' => TRUE),
                'флаг' => array('type' => 'TEXT', 'null' => TRUE),
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
            // create table
            $this->forge->createtable('country', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('событие'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_страны' => array('type' => 'INT', 'null' => FALSE),                
                'описание' => array('type' => 'TEXT', 'null' => TRUE),
                'дата' => array('type' => 'DATE', 'null' => TRUE)
            ));
            $this->forge->addForeignKey('id_страны','country','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('событие', TRUE);
        }



        // activity_type
        if (!$this->db->tableexists('товарооборот'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_страны1' => array('type' => 'INT', 'null' => FALSE),
                'id_страны2' => array('type' => 'INT', 'null' => FALSE),
                'год' => array('type' => 'INT', 'null' => FALSE),
                'экспорт_из_страны1_в_страну2' => array('type' => 'FLOAT', 'null' => FALSE),
                'экспорт_из_страны2_в_страну1' => array('type' => 'FLOAT', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_страны1','country','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_страны2','country','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('товарооборот', TRUE);
        }

        if (!$this->db->tableexists('экономика'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_страны' => array('type' => 'INT', 'null' => FALSE),
                'год' => array('type' => 'INT', 'null' => FALSE),
                'ВВП' => array('type' => 'FLOAT', 'null' => FALSE),
                'ВВП/чел' => array('type' => 'FLOAT', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_страны','country','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('экономика', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{
         $this->forge->droptable('событие');
         $this->forge->droptable('страна');
         $this->forge->droptable('товарооборот');
         $this->forge->droptable('экономика');
	}
}
