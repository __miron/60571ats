<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Countrysystem extends Seeder
{
	public function run()
	{
       $data = [
                'наименование' => 'Россия',
                'столица'=>'Москва',
                'население' => 146748590,
                'площадь' => 17125200,
                'флаг' => 'https://www.flaticon.com/svg/vstatic/svg/555/555451.svg?token=exp=1615488732~hmac=4f6d4032576e67bce9ae22258f3fa53c',       
        ];
        $this->db->table('country')->insert($data);

        $data = [
            
            'id_страны1'=> 1,
            'id_страны2' => 1,
            'год' => 2018,
            'экспорт_из_страны1_в_страну2' => 56128600000,
            'экспорт_из_страны2_в_страну1' => 53212600000,
        ];
        $this->db->table('товарооборот')->insert($data);
        
        $data = [
                
                'id_страны' => 1,
                'год' => 1998,
                'ВВП' => 2629600000,
                'ВВП/чел' => 679.61,
        ];
        $this->db->table('экономика')->insert($data);

        $data = [
            
            'id_страны'=> 1,
            'описание' => 'Обувь, головные уборы и зонты.',
            'дата' => '2021-01-25',
        ];
        $this->db->table('событие')->insert($data);
	}
}
