-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 13 2021 г., 02:39
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571ats`
--

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$omFooHhSl.3srmfu4xdiOucvg/yPeumRTE72asZgOmsIK1H3a0zw2', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1615571937, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', 'fargus286@gmail.com', '$2y$10$4ysN9mgipPDVqO31gcHqkOl.l6LxqppIIYYOeXGCS9UbQHPldrK42', 'fargus286@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1615551711, NULL, 1, 'Тигран', 'Акобян', '', ''),
(3, '127.0.0.1', 'matrixn257@gmail.com', '$2y$10$9t16owcnDSEBZ/7Sag9/vuwZo.oCwf4OkjmWjJ/q9T064hklkRyEa', 'matrixn257@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1615552465, 1615552492, 1, 'Tigran', 'Akobyan', 'SurGU', '89995711045');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `событие`
--

CREATE TABLE `событие` (
  `id` int NOT NULL,
  `id_страны` int NOT NULL,
  `описание` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `дата` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `событие`
--

INSERT INTO `событие` (`id`, `id_страны`, `описание`, `дата`) VALUES
(1, 3, 'Обувь, головные уборы и зонты. ', '2021-01-25'),
(2, 1, 'Пищевые продукты, напитки и табак. ', '2020-10-15'),
(3, 12, 'Пластмасса, каучук и резина. ', '2020-09-15'),
(4, 9, 'Пищевые продукты, напитки и табак. ', '2021-02-05'),
(5, 6, 'Оружие и боеприпасы.', '2020-11-04'),
(6, 7, 'Пищевые продукты, напитки и табак. ', '2020-01-15'),
(7, 2, 'Обувь, головные уборы и зонты. ', '2020-04-12'),
(8, 8, 'Пластмасса, каучук и резина. ', '2021-01-12'),
(9, 11, 'Покупка драгоценностей. ', '2021-02-25'),
(10, 11, 'Обувь, головные уборы и зонты. ', '2020-03-16'),
(11, 5, 'Покупка драгоценностей. ', '2020-06-25'),
(12, 4, 'Оружие и боеприпасы. ', '2021-01-02');

-- --------------------------------------------------------

--
-- Структура таблицы `страна`
--

CREATE TABLE `страна` (
  `id` int NOT NULL,
  `наименование` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `столица` text NOT NULL,
  `население` int NOT NULL,
  `площадь` float NOT NULL,
  `флаг` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `страна`
--

INSERT INTO `страна` (`id`, `наименование`, `столица`, `население`, `площадь`, `флаг`) VALUES
(1, 'Россия', 'Москва', 146748590, 17125200, 'https://www.flaticon.com/svg/vstatic/svg/555/555451.svg?token=exp=1615488732~hmac=4f6d4032576e67bce9ae22258f3fa53c'),
(2, 'США', 'Вашингтон', 329484123, 9826680, 'https://www.flaticon.com/svg/vstatic/svg/555/555526.svg?token=exp=1615488807~hmac=bd680abc67c51a9097d50516ca162e8f'),
(3, 'Япония', 'Токио', 125570000, 377944, 'https://www.flaticon.com/svg/vstatic/svg/555/555646.svg?token=exp=1615488848~hmac=54adb545bf6f019af35856432e5601a4'),
(4, 'Китай', 'Пекин', 1439323776, 9596960, 'https://www.flaticon.com/svg/vstatic/svg/555/555418.svg?token=exp=1615488832~hmac=981e140088c7b35a03dc9352ddecb753'),
(5, 'Индия', 'Нью-Дели', 1326093247, 3287260, 'https://www.flaticon.com/svg/vstatic/svg/555/555462.svg?token=exp=1615488714~hmac=72c6f3785657ab2798a92c8fd5ec36bb'),
(6, 'Польша', 'Варшава', 38382576, 312696, 'https://www.flaticon.com/svg/vstatic/svg/939/939676.svg?token=exp=1615488926~hmac=9a97d6453cbf5b1fbef6876c2f7989ed'),
(7, 'Германия', 'Берлин', 83149300, 357578, 'https://www.flaticon.com/svg/vstatic/svg/555/555613.svg?token=exp=1615488751~hmac=4493f13c210736ed1af335131013d0b3'),
(8, 'Великобритания', 'Лондон', 67886004, 242495, 'https://www.flaticon.com/svg/vstatic/svg/555/555527.svg?token=exp=1615488650~hmac=d141fe463117fcda93cc5fca199deec2'),
(9, 'Саудовская Аравия', 'Эр-Рияд', 34218169, 2149690, 'https://www.flaticon.com/svg/vstatic/svg/206/206719.svg?token=exp=1615488318~hmac=929aab60722bef2c488778195ddcf6fc'),
(10, 'Армения', 'Ереван', 2956900, 29743, 'https://www.flaticon.com/svg/vstatic/svg/555/555558.svg?token=exp=1615488672~hmac=b951bea799b7e851f48832a9f2841298'),
(11, 'Азербайджан', 'Баку', 10127874, 86600, 'https://www.flaticon.com/svg/vstatic/svg/555/555554.svg?token=exp=1615488609~hmac=605f2d15191a702dc7fd5ebf0e56f129'),
(12, 'Грузия', 'Тбилиси', 3716858, 69700, 'https://www.flaticon.com/svg/vstatic/svg/555/555423.svg?token=exp=1615488585~hmac=fe62953c490920a867347a4287f39ecc'),
(13, 'Казахстан', 'Астана', 18877128, 2724900, 'https://www.flaticon.com/svg/vstatic/svg/555/555645.svg?token=exp=1615488632~hmac=11ec04f423aa1cd821af3ff06a1ee424'),
(14, 'Турция', 'Анкара', 83314352, 783356, 'https://www.flaticon.com/svg/vstatic/svg/555/555560.svg?token=exp=1615488694~hmac=77301c761ed5ae69da4de7bc87597c3b'),
(15, 'Вьетнам', 'Ханой', 94660000, 331210, 'https://www.flaticon.com/svg/vstatic/svg/555/555515.svg?token=exp=1615488789~hmac=26120b847278a8300a034aa6dbdb448a');

-- --------------------------------------------------------

--
-- Структура таблицы `товарооборот`
--

CREATE TABLE `товарооборот` (
  `id` int NOT NULL,
  `id_страны1` int NOT NULL,
  `id_страны2` int NOT NULL,
  `год` year NOT NULL,
  `экспорт_из_страны1_в_страну2` float NOT NULL,
  `экспорт_из_страны2_в_страну1` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `товарооборот`
--

INSERT INTO `товарооборот` (`id`, `id_страны1`, `id_страны2`, `год`, `экспорт_из_страны1_в_страну2`, `экспорт_из_страны2_в_страну1`) VALUES
(14, 1, 4, 2018, 56128600000, 53212600000),
(15, 1, 4, 2019, 57136400000, 54128500000),
(16, 1, 4, 2020, 57095200000, 54014000000),
(17, 2, 3, 2018, 54100100000, 36001300000),
(18, 2, 3, 2019, 54701400000, 37245600000),
(19, 2, 3, 2020, 56286600000, 38623000000),
(20, 2, 9, 2018, 6125690000, 6514870000),
(21, 2, 9, 2019, 6378950000, 6645620000),
(22, 2, 9, 2020, 64245600000, 67891400000),
(23, 10, 12, 2018, 9256880000, 10154600000),
(24, 10, 12, 2019, 9956870000, 10856700000),
(25, 10, 12, 2020, 10562500000, 11545900000),
(26, 6, 7, 2019, 52548600000, 44981700000);

-- --------------------------------------------------------

--
-- Структура таблицы `экономика`
--

CREATE TABLE `экономика` (
  `id` int NOT NULL,
  `id_страны` int NOT NULL,
  `год` year NOT NULL,
  `ВВП` float NOT NULL,
  `ВВП/чел` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `экономика`
--

INSERT INTO `экономика` (`id`, `id_страны`, `год`, `ВВП`, `ВВП/чел`) VALUES
(1, 1, 1998, 2629600000, 679.61),
(3, 1, 1999, 4823200000, 701.23),
(4, 1, 2000, 7305600000, 780.25),
(5, 1, 2001, 8943600000, 825.61),
(6, 1, 2002, 10830500000, 900.29),
(7, 1, 2003, 13080200000, 925.11),
(8, 1, 2004, 17042000000, 950.83),
(9, 1, 2005, 21020000000, 1001.92),
(10, 1, 2006, 26917200000, 1068.35),
(11, 1, 2007, 33247500000, 1100.26),
(12, 1, 2008, 41276800000, 1205.19),
(13, 1, 2009, 38807200000, 1167.64),
(14, 1, 2010, 46308500000, 1189.21),
(15, 1, 2011, 60114000000, 1286.63);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Индексы таблицы `событие`
--
ALTER TABLE `событие`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_страны` (`id_страны`);

--
-- Индексы таблицы `страна`
--
ALTER TABLE `страна`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `товарооборот`
--
ALTER TABLE `товарооборот`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_страны1` (`id_страны1`,`id_страны2`),
  ADD KEY `id_страны2` (`id_страны2`);

--
-- Индексы таблицы `экономика`
--
ALTER TABLE `экономика`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_страны` (`id_страны`) USING BTREE;

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `событие`
--
ALTER TABLE `событие`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `страна`
--
ALTER TABLE `страна`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `товарооборот`
--
ALTER TABLE `товарооборот`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `экономика`
--
ALTER TABLE `экономика`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `событие`
--
ALTER TABLE `событие`
  ADD CONSTRAINT `событие_ibfk_1` FOREIGN KEY (`id_страны`) REFERENCES `страна` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `товарооборот`
--
ALTER TABLE `товарооборот`
  ADD CONSTRAINT `товарооборот_ibfk_1` FOREIGN KEY (`id_страны1`) REFERENCES `страна` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `товарооборот_ibfk_2` FOREIGN KEY (`id_страны2`) REFERENCES `страна` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `экономика`
--
ALTER TABLE `экономика`
  ADD CONSTRAINT `экономика_ibfk_1` FOREIGN KEY (`id_страны`) REFERENCES `страна` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
